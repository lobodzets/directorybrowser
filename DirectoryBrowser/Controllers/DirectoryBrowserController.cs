﻿using System;
using System.Collections.Generic;
using DirectoryBrowser.Models;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace DirectoryBrowser.Controllers
{

    public class DirectoryBrowserController : ApiController
    {
        [Route("api/directoryBrowser/next")]
        public IHttpActionResult GetNext([FromUri] string currentPath = "", [FromUri] string path = "")
        {
            var newPath = Path.Combine(currentPath, path);
            return Ok<DirectoryInfoModel>(new DirectoryInfoModel(newPath));
        }

        [Route("api/directoryBrowser/previous")]
        public IHttpActionResult GetPrevious([FromUri] string currentPath = "")
        {
            var parent = new DirectoryInfo(currentPath).Parent;
            if (parent == null)
            {
                return Ok<DirectoryInfoModel>(null);
            }
            else
            {
                var newPath = parent.FullName;
                return Ok<DirectoryInfoModel>(new DirectoryInfoModel(newPath));
            }

        }

        [Route("api/directoryBrowser/sizeRange")]
        public IHttpActionResult GetSizeRange([FromUri] string path)
        {
            if (!Directory.Exists(path)) return NotFound();
            
            return Ok<SizeRangeModel>(new SizeRangeModel(path));

        }
        
    }
}
