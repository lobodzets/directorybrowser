﻿using DirectoryBrowser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DirectoryBrowser.Controllers
{
    public class DrivesController : ApiController
    {
        DriveModel DriveModel = new DriveModel();

        public IHttpActionResult Get()
        {
            return Ok<List<DriveModel.Drive>>(DriveModel.GetAll());
        }
    }
}
