﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DirectoryBrowser.Models
{
    public class DirectoryInfoModel
    {
        public string Path { get; set; }
        public List<DirectoryModel> Directories;
        public List<FileModel> Files;          

        public DirectoryInfoModel(string Path)
        {
            this.Path = Path;
            this.Directories = new List<DirectoryModel>();
            this.Files = new List<FileModel>();            
            
            this.GetDirectoriesFromFS();
            this.GetFilesFromFS();            
        }             

        private void GetDirectoriesFromFS()
        {
            this.Directories.Clear();
            var Directories = Directory.EnumerateDirectories(this.Path);
            foreach (var directoryPath in Directories)
            {
                var directory = new DirectoryInfo(System.IO.Path.GetDirectoryName(directoryPath + System.IO.Path.DirectorySeparatorChar));
                this.Directories.Add(new DirectoryModel(directory));
            }
        }

        private void GetFilesFromFS()
        {
            this.Files.Clear();
            var Files = Directory.EnumerateFiles(this.Path);
            foreach (var filePath in Files)
            {
                var file = new FileInfo(filePath);
                this.Files.Add(new FileModel(file));
            }
        }       
    }
    

    
}