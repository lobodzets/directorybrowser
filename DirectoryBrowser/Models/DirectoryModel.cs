﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DirectoryBrowser.Models
{
    public class DirectoryModel
    {
        public string Name { get; set; }

        public DirectoryModel(DirectoryInfo directory)
        {
            this.Name = directory.Name;
        }
    }
}