﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DirectoryBrowser.Models
{
    public class DriveModel
    {
        public List<Drive> GetAll()
        {
            List<Drive> result = new List<Drive>();

            var driveList = DriveInfo.GetDrives().Where(d => d.IsReady).ToList();
            foreach (DriveInfo drive in driveList)
            {
                result.Add(new Drive(drive));
            }

            return result;
        }
        
        public class Drive
        {
            public string Name { get; set; }

            public Drive(DriveInfo driveInfo)
            {
                this.Name = driveInfo.Name; 
            }
        }
    }
}