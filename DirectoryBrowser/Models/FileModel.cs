﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DirectoryBrowser.Models
{
    public class FileModel
    {
        public string Name { get; set; }

        public FileModel(FileInfo file)
        {
            this.Name = file.Name;
        }
    }
}