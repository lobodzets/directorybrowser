﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DirectoryBrowser.Models
{
    public class SizeRangeModel
    {
        private string Path { get; set; }
        public int Less10Mb { get; set; }
        public int From10to50Mb { get; set; }
        public int More100Mb { get; set; }

        public SizeRangeModel(string Path)
        {
            this.Path = Path;
            this.Less10Mb = 0;
            this.From10to50Mb = 0;
            this.More100Mb = 0;

            this.calculate();
        }
       
        private void calculate()
        {
            var directory = new DirectoryInfo(this.Path);

            var files = SafeFileEnumerator.EnumerateFiles(this.Path, "*", SearchOption.AllDirectories).Take(100);
            this.Less10Mb = files.Where(f => this.ConvertBytesToMegabytes((new FileInfo(f)).Length) <= 10).Count();
            this.From10to50Mb = files.Where(f => this.ConvertBytesToMegabytes((new FileInfo(f)).Length) > 10 && this.ConvertBytesToMegabytes((new FileInfo(f)).Length) <= 50).Count();
            this.More100Mb = files.Where(f => this.ConvertBytesToMegabytes((new FileInfo(f)).Length) >= 100).Count();
        }

        private double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

    }

    public static class SafeFileEnumerator
    {        
        public static IEnumerable<string> EnumerateFiles(string path, string searchPattern, SearchOption searchOpt)
        {
            try
            {
                var dirFiles = Enumerable.Empty<string>();
                if (searchOpt == SearchOption.AllDirectories)
                {
                    dirFiles = Directory.EnumerateDirectories(path)
                                        .SelectMany(x => EnumerateFiles(x, searchPattern, searchOpt));
                }
                return dirFiles.Concat(Directory.EnumerateFiles(path, searchPattern));
            }
            catch (UnauthorizedAccessException ex)
            {
                return Enumerable.Empty<string>();
            }
        }
    }
}