﻿angular.module('directoryBrowser').directive('directoryBrowser', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            
        },
        templateUrl: '/SPA/DirectoryBrowser/_directoryBrowser.html',
        controller: ['$scope', '$parse', 'DirectoryBrowserService', function ($scope, $parse, DirectoryBrowserService) {
            var self = this;

            self.refreshDrives = function () {
                DirectoryBrowserService.getDrives(function (drives) {
                    $scope.drives = drives;
                });
            }
            
            $scope.selectDrive = function (drive) {                                           
                $scope.getNextDirectory(drive.Name);
            }

            $scope.getNextDirectory = function (path) {
                DirectoryBrowserService.getNextDirectory({ currentPath: $parse('directoryInfo.Path')($scope), path: path }, function (info) {
                    $scope.directoryInfo = info;
                    self.getSizeRange();
                });
            }

            $scope.getPreviousDirectory = function () {
                DirectoryBrowserService.getPreviousDirectory({ currentPath: $parse('directoryInfo.Path')($scope) }, function (info) {
                    $scope.directoryInfo = info;                    
                    $scope.directoryInfo ? self.getSizeRange() : $scope.sizeRange = {};
                });
            }

            self.getSizeRange = function () {                
                DirectoryBrowserService.getSizeRange({ path: $parse('directoryInfo.Path')($scope)}, function (info) {
                    $scope.sizeRange = info;
                })
            }

           
            self.refreshDrives();

        }]
    };
})