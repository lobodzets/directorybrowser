﻿angular.module('directoryBrowser').service('DirectoryBrowserService', ['$http', function ($http) {
    var self = this;

    self.getDrives = function (callback) {
        $http({
            method: 'GET',
            url: '/api/drives'
        }).then(function successCallback(response) {            
            if (callback) callback(response.data);
        }, function errorCallback(response) {
            throw new Error("GetDrives fails. Request error: Status: " + response.status + ", Text: " + response.statusText);
        });
    }
    
    self.getNextDirectory = function (params, callback) {
        $http({
            method: 'GET',
            params: {
                currentPath: params.currentPath,
                path: params.path
            },
            url: '/api/directoryBrowser/next'
        }).then(function successCallback(response) {            
            if (callback) callback(response.data);
        }, function errorCallback(response) {
            throw new Error("GetNextDirectory fails. Request error: Status: " + response.status + ", Text: " + response.statusText);
        });                
    }

    self.getPreviousDirectory = function (params, callback) {
        $http({
            method: 'GET',
            params: {
                currentPath: params.currentPath
            },
            url: '/api/directoryBrowser/previous'
        }).then(function successCallback(response) {
            if (callback) callback(response.data);
        }, function errorCallback(response) {
            throw new Error("GetPreviousDirectory fails. Request error: Status: " + response.status + ", Text: " + response.statusText);
        });
    }

    self.getSizeRange = function (params, callback) {
        $http({
            method: 'GET',
            params: {
                path: params.path
            },
            url: '/api/directoryBrowser/sizeRange'
        }).then(function successCallback(response) {
            if (callback) callback(response.data);
        }, function errorCallback(response) {
            throw new Error("GetSizeRange fails. Request error: Status: " + response.status + ", Text: " + response.statusText);
        });
    }
}]);